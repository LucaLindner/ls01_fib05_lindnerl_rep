﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
         
    	
    	while(true) {
	
	       double zuZahlenderBetrag; 
	       double rückgabebetrag;
	            
	               
	       zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       
	       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	
	       fahrkartenAusgeben();
	
	       rueckgeldAusgeben(rückgabebetrag);
	           
	
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n"
	                          + "--------------------------------------------");
    	} 
    }
    
  
    
    //--------------------------------------------------------------------------
    static double fahrkartenbestellungErfassen(){
    	
    	double[] priceList = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9}; 
    	   	
    	int input = 0;
    	double endPrice = 0;
    	
    	while(input != 11) {
    		
    		fahrkartenMenü();
    		
    		while(true) {
    			try {
    				input = tastatur.nextInt();
    				
    				if(input != 11 && input <= 0 || input > 11) {
    					throw new Exception();
    				}
    				
    				break;
    			}catch (Exception e) {
    				log("Bitte gebe eine gültige Zahl ein :)");
    				tastatur.nextLine();
    			}
    		}
    		
    		log("Ihre Wahl: " + input);
    		
    		if(input != 11) {
    			log("\nAnzahl der Tickets:");
    			int count = 0;
    			
    			while(count <= 0) {   
		     	   
		     	   try {
		     		  count = tastatur.nextInt();
			
		     		   if(count <= 0  || count > 10) {
		     			  throw new Exception();
		     		   } 
		           	}catch (Exception e){
		           		//java.util.InputMismatchException
		           		System.out.println("Bitte gebe eine gültige Zahl ein (zwischen 1 und 10)");
		           		count = 0;
		           		tastatur.nextLine();
		           	}
		     	   

    		     }
    			
    			endPrice += (priceList[input-1]*count);
    			
    		}
    		
    		
    		
    	}	

    	   	
    	return endPrice;
    }
    
    // Geldeinwurf
    // -----------
    static double fahrkartenBezahlen(double zuZahlen) {
    	
        double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
    	
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   String outputString;
     	   double output = (zuZahlen - eingezahlterGesamtbetrag);
     	   
     	   if(String.valueOf(output).length() <= 4) {
     		   outputString = output + "0";
     	   }else if(String.valueOf(output).length() > 4) {
     		   outputString = String.valueOf(output).substring(0, 4);
     	   }else {
     		   outputString = String.valueOf(output);
     	   }
     	   
     	  // String outputString = String.valueOf(output).length() <= 3 ?  : output + "";	  
     	   
     	   //Format Euro 
     	   System.out.println("Noch zu zahlen: " + outputString + " €");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 10 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    
    static void fahrkartenMenü() {
    	String[] nameList = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] priceList = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9}; 
    	
    	log("Wählen Sie bitte aus:");
    	
    	for(int i = 0; i < nameList.length; i++) {
    		System.out.printf("%.30s", "   (" + (i+1) + ") " + nameList[i]);
    		System.out.printf("%20s %n", priceList[i] + "0 €"); 		
    	}
    	
    	log("   ("+ (nameList.length+1) +") Bezahlen");
    	
    }
    

    
    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    static void rueckgeldAusgeben(double rückgeld) {
    	
    	if(rückgeld > 0.0)
        {
     	   
     	   // System.out.println("\n--------------------\nRückgabevetrag: " + rückgabebetrag + "\n--------------------\n");
     	   
     	   String outputString;
     	   
     	   if(String.valueOf(rückgeld).length() <= 4) {
     		   outputString = rückgeld + "0";
     	   }else if(String.valueOf(rückgeld).length() > 4) {
     		   
     		   //nicht korekt 		 
     		   outputString = String.valueOf(rückgeld).substring(0, 4);
     	   }else {
     		   outputString = String.valueOf(rückgeld);
     	   }
     	   
     	   
     	   System.out.println("Der Rückgabebetrag in Höhe von " + outputString + " €");
     	   System.out.println("Hinweis! Der oben genannte Betrag könnte eventuell von dem Tatsächlichen Rückbetrag abweichen!");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   
            while(rückgeld >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "EURO");
         	    rückgeld = Math.round((rückgeld  - 2)*100D)/100D;
            }
            while(rückgeld >= 1.0) // 1 EURO-Münzen
            {

            	muenzeAusgeben(1, "EURO");
         	 rückgeld = Math.round((rückgeld  - 1)*100D)/100D;

            }
            while(rückgeld >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "CENT");
         	 rückgeld = Math.round((rückgeld - 0.5)*100D)/100D;
            }
            while(rückgeld >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "CENT");
         	 rückgeld = Math.round((rückgeld  - 0.2)*100D)/100D;
            }
            while(rückgeld >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "CENT");
         	 rückgeld = Math.round((rückgeld  - 0.1)*100D)/100D;
            }
            while(rückgeld >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "CENT");
         	 rückgeld = Math.round((rückgeld  - 0.05)*100D)/100D;
            }
        }
    }
    
    static void fahrkartenAusgeben() {
    	
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
 	
    }
    
    static void muenzeAusgeben(int betrag, String einheit) {
    	
    	System.out.println(betrag + " " + einheit);
    	
    }
    
    static void warte(int millisekunde) {
   	 
  	  try {
  			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
  	
    }
    
    static void log(String value) {
    	System.out.println(value);
     }
    
    static void printCoin(int value, String currency) {
    	System.out.printf( "", "    * * *");
    	System.out.printf( "", "  *       *");
    	System.out.printf( "", "*     " + value + "     *");
    	System.out.printf( "", "*     " + currency + "     *");
    	System.out.printf( "", "  *       *");
    	System.out.printf( "", "    * * *");
    	
    }
}