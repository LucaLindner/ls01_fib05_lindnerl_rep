
public class Aufgabe3 {
	
	
	
	public static void main(String[] args) {
		
		int[] Fahrenheit = {-20, -10, 0, 20, 30};
		double[] Celsius = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s", "Celsius");
		System.out.println("\n------------------------");
		
		for(int i = 0; i < Fahrenheit.length; i++) {
			System.out.printf("%-12s", Fahrenheit[i]);
			System.out.print("|");
			System.out.printf("%10.6s", Celsius[i]);
			System.out.print("\n");
		}
	}
	
}
