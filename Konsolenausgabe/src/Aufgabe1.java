
public class Aufgabe1 {

	static Starfield SF = new Starfield() ;
	
	public static void main(String[] args) {
		SF.draw();

	}
	
	public static class Starfield{
	
		
		void draw() {
			System.out.println("	   **");
			System.out.println("	*      *");
			System.out.println("	*      *");
			System.out.println("	   **");
			
		}
	} 

}
