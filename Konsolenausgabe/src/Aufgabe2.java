
public class Aufgabe2 {

	public static void main(String[] args) {
			
			String inhalt = "";
			int[] arr = {1, 1, 2, 6, 24, 120};
			
			for(int i = 0; i < arr.length; i++) {
				System.out.printf("%-5s", "!" + i);
				System.out.print("= ");
				
				if(i != 0) {
					if(i == 1) {
						inhalt += i;
					}else {
						inhalt += " * " + i;		
					}
							
				}
				
				System.out.printf("%-19s", inhalt);
				System.out.print(" = ");
				System.out.printf("%4s", arr[i]);
				System.out.print("\n");
				
			}
	
		}
}
